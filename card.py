suits_name = ['Spades', 'Diamonds', 'Hearts', 'Clubs']
suits_symbols = ['♠', '♦', '♥', '♣']

card_values = {
        'Ace': 11, 
        '2': 2,
        '3': 3,
        '4': 4,
        '5': 5,
        '6': 6,
        '7': 7,
        '8': 8,
        '9': 9,
        '10': 10,
        'Jack': 10,
        'Queen': 10,
        'King': 10
    }
    
class Card():
    '''def __init__(self, card):
        pass'''
    suit = suits_name
    rank = card_values  
    def compare(self, card):
        if self > card:
            return 1
        elif self < card:
            return -1
        elif self == card:
            return 0
        else:
            pass
cards = Card()
print(cards.compare)