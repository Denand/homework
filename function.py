# Задание: Задачи по функциям

def fib(n):
    if n==1 or n==2:
        return 1
    return fib(n-1) + fib(n-2)
print(fib(6))


a = (26, 27, 42, 70, 51)
def func_min(*args):
    z = min(a)
    return z
print(func_min(a))


def kw(*args,**kwargs):
    return args, kwargs
print(kw(1, 2, c = 3, d = 4))


def func(*args):
    res = args[0]
    for arg in args:
        if arg < res:
            if arg > 0:
                res = arg
            else:
                pass
    return res
print(func(60, 19, 50, -4))