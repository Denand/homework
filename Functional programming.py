# Функциональное программирование

def function_double(n):
    return list(map(lambda x: x * 2, n))
double = [2, 8, 25]
print(function_double(double))

def function_elements(n, n0, n1):
    return list(map(lambda x, y, z: x * y * z, n, n0, n1))
z = [2, 4]
e = [6, 8]
t = [10, 12]
print(function_elements(z, e, t))

def function_length(n):
    return list(map(lambda x: len(x), n))
length = ['One', 'Two', 'Three']
print(function_length(length))

def function_even(n):
    return list(filter(lambda x: n.index(x) % 2 == 0, n))
even = [1, 2, 3, 4, 5, 6, 7, 8]
print(function_even(even))

def function_nonempty(n):
    return list(filter(lambda x: x != '', n))
nonempty = ['', 'There', 'are', '', 'empty', '', 'elements', 'here', '']
print(function_nonempty(nonempty))

def function_zip(n, n0, n1):
    return list(zip(n, n0, n1))
zip_1 = [1, 2, 3, 4]
zip_2 = [5, 6, 7, 8]
zip_3 = [9, 10, 11, 12]
print(function_zip(zip_1, zip_2, zip_3))

def function_delements(n, n0):
    return list(zip(n, map(lambda x: x * 2, n0)))
k = [1, 2, 3, 4]
u = [5, 6, 7, 8]
print(function_delements(k, u))