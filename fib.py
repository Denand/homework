# Числа Фибоначчи #

n = int(input("Enter fib number: "))
fib_1 = 1
fib_2 = 1
i = 2

if n == 1:
    print("1")
elif n == 2:
    print("1")
else:
    while n > i:
        fib = fib_1 + fib_2
        fib_1 = fib_2
        fib_2 = fib
        i += 1
    print (fib)